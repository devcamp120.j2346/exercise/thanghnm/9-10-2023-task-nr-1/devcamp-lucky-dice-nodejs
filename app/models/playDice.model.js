//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const voucherSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: false,
    },
    discount: {
        required: true,
        type: Number,
    },
    note: {
        type: String,
    }
}, {
    timestamps: true
}
)
const prizeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: false,
    },
    description: {
        type: String,
    }
}, {
    timestamps: true
}
)
const playSchema = new Schema({
    voucher: voucherSchema,
    prize: prizeSchema,
    dice: {
        type: Number,
        required: true
    }
}
)
// Biên dịch Schema
module.exports = mongoose.model("Play", playSchema)