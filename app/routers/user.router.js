const express = require("express");
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require("../controllers/user.controller");

const router = express.Router();

router.post("/", createUser)
// Router Get all
router.get("/", getAllUser)
// Router Get by Id
router.get("/:userId", getUserById)
// Router Update by Id
router.put("/:userId", updateUserById)
// Router Delete by Id
router.delete("/:userId", deleteUserById)
//
module.exports = router