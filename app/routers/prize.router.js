const express = require("express");
const { createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById } = require("../controllers/prize.controller");
const router = express.Router();
// Router Tạo
router.post("/", createPrize)
// Router Get all
router.get("/", getAllPrize)
// Router Get by Id
router.get("/:prizeId", getPrizeById)
// Router Update by Id
router.put("/:prizeId", updatePrizeById)
// Router Delete by Id
router.delete("/:prizeId", deletePrizeById)

//
module.exports = router