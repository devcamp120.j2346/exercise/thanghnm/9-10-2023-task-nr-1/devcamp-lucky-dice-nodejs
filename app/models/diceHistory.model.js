//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const historySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: true
    },
    dice: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
}
)
// Biên dịch Schema
module.exports = mongoose.model("DiceHistory", historySchema)