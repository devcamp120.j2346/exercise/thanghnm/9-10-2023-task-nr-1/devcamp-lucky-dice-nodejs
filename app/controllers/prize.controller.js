//Import Model
const prizeModel = require("../models/prize.model")
const mongoose = require("mongoose")
// Hàm tạo 
const createPrize = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { name, description } = req.body
    // B2 Kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }

    // B3 Xử lý 
    let newObj = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    }
    try {
        const result = await prizeModel.create(newObj);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm get all
const getAllPrize = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await prizeModel.find();
    return res.status(200).json({
        result
    });
}
// Hàm get bằng Id
const getPrizeById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var prizeId = req.params.prizeId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await prizeModel.findById(prizeId);
    return res.status(200).json({
        result
    });
}
const updatePrizeById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var prizeId = req.params.prizeId
    const { name, description } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "VoucherId is invalid"
        })
    }
    // B3 Xử lý 
    let updated = {
        description,
        name
    }
    const result = await prizeModel.findByIdAndUpdate(prizeId, updated);
    return res.status(200).json({
        status: "Update Voucher successfully",
        result
    });
}
// Hàm xóa  bằng Id
const deletePrizeById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var prizeId = req.params.prizeId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await prizeModel.findByIdAndDelete(prizeId);
    return res.status(204).json({
        status: "Delete Voucher successfully"
    });
}
// Export
module.exports = {
    createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById
}