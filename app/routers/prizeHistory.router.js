const express = require("express");
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById } = require("../controllers/prizeHistory.controller");
const { getAllDiceHistoryOfUser } = require("../controllers/diceHistory.controller");
const router = express.Router();

// Router Tạo
router.post("/", createPrizeHistory)
// Router Get all
router.get("/all/", getAllPrizeHistory)
router.get("/", getAllDiceHistoryOfUser)
// Router Get by Id
router.get("/:prizeHistoryId", getPrizeHistoryById)
// Router Update by Id
router.put("/:prizeHistoryId", updatePrizeHistoryById)
// Router Delete by Id
router.delete("/:prizeHistoryId", deletePrizeHistoryById)

module.exports = router