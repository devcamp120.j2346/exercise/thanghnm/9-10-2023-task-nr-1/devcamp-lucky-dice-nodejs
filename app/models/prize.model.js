//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const prizeSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
    }
}, {
    timestamps: true
}
)
// Biên dịch Schema
module.exports = mongoose.model("Prize", prizeSchema)