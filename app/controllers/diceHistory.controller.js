//Import Model
const diceHistoryModel = require("../models/diceHistory.model")
const userModel = require("../models/user.model")
const playDiceModel = require("../models/playDice.model")
const voucherModel = require("../models/voucher.model")
const voucherHistoryModel = require("../models/voucherHistory.model")
const prizeModel = require("../models/prize.model")
const prizeHistoryModel = require("../models/prizeHistory.model")

const mongoose = require("mongoose")
// Hàm tạo Dice History
const createDiceHistory = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { user, dice } = req.body
    // B2 Kiểm tra dữ liệu
    if (!user) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is required"
        })
    }
    if (!dice) {
        return res.status(400).json({
            status: "Bad request",
            message: "dice is required"
        })
    }
    if (dice == !Number) {
        return res.status(400).json({
            status: "Bad request",
            message: "dice is Number"
        })
    }
    if (dice < 1 || dice > 6) {
        return res.status(400).json({
            status: "Bad request",
            message: "dice from 1 to 6"
        })
    }
    // B3 Xử lý 
    let newDice = {
        _id: new mongoose.Types.ObjectId(),
        dice,
        user
    }
    try {
        const result = await diceHistoryModel.create(newDice);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm get tất cả history
const getAllDiceHistory = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await diceHistoryModel.find();
    return res.status(200).json({
        result
    });
}
// Hàm get tất cả history
const getAllDiceHistoryOfUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const username = req.query.username
    const condition = {}
    condition.username = username
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const findUser = await userModel.findOne(condition)
    if (findUser === null) {
        result = []
        return res.status(404).json({
            result: result
        })
    }
    else {
        const condition = {}
        condition.user = findUser._id
        const result = await diceHistoryModel.find(condition);
        return res.status(200).json({
            result
        });
    }

}
// Hàm get bằng Id
const getDiceHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var historyId = req.params.historyId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await diceHistoryModel.findById(historyId);
    return res.status(200).json({
        result
    });
}
const updateDiceHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var historyId = req.params.historyId
    const { user, dice } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "VoucherId is invalid"
        })
    }
    // B3 Xử lý 
    let updated = {
        dice,
        user
    }
    const result = await diceHistoryModel.findByIdAndUpdate(historyId, updated);
    return res.status(200).json({
        status: "Update Voucher successfully",
        result
    });
}
// Hàm xóa History bằng Id
const deleteHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var historyId = req.params.historyId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await diceHistoryModel.findByIdAndDelete(historyId);
    return res.status(204).json({
        status: "Delete Voucher successfully"
    });
}
const playDice = async (req, res) => {
    const { firstname, lastname, username } = req.body
    if (!firstname) {
        return res.status(400).json({
            message: "firstname is required"
        })
    }
    if (!lastname) {
        return res.status(400).json({
            message: "lastname is required"
        })
    }
    if (!username) {
        return res.status(400).json({
            message: "username is required"
        })
    }
    const gDataObject = {
        firstname,
        lastname,
        username
    }
    const condition = {}
    if (username) {
        condition.username = username
    }
    const findUser = await userModel.findOne(condition)
    // console.log(findUser)
    // Nếu user chưa tồn tại
    if (findUser === null) {
        const createUser = await userModel.create(gDataObject)
        const createDiceHistory = await diceHistoryModel.create({
            user: createUser._id,
            dice: rollDice()
        })
        // Nếu xúc sắc đổ ra lớn hơn 3
        if (createDiceHistory.dice > 3) {
            // Get the count of all users
            const getAllVoucher = await voucherModel.count().exec()
            // Get a random entry
            var random = Math.floor(Math.random() * getAllVoucher)
            // Again query all users but only fetch one offset by our random #
            const getRandomVoucher = await voucherModel.findOne().skip(random).exec()
            const postVoucherHistory = await voucherHistoryModel.create({
                user: createUser._id,
                voucher: getRandomVoucher
            })
            const play = {
                dice: createDiceHistory.dice,
                voucher: getRandomVoucher,
                prize: null
            }
            const result = await playDiceModel.create(play)
            // Trả ra kêt quả Json
            return res.status(200).json({
                result
            })
        }
        // Nếu xúc sắc đổ ra bé hơn hoặc bằng 3
        else {
            const play = {
                dice: createDiceHistory.dice,
                voucher: null,
                prize: null
            }
            const result = await playDiceModel.create(play)
            // Trả ra kêt quả Json
            return res.status(200).json({
                result
            })
        }

    }
    // Nếu user đã tồn tại
    else {
        const createDiceHistory = await diceHistoryModel.create({
            user: findUser._id,
            dice: rollDice()
        })
        // Nếu xúc sắc đổ ra lớn hơn 3
        if (createDiceHistory.dice > 3) {
            // Get the count of all users
            const getAllVoucher = await voucherModel.count().exec()
            // Get a random entry
            var random = Math.floor(Math.random() * getAllVoucher)
            // Again query all users but only fetch one offset by our random #
            const getRandomVoucher = await voucherModel.findOne().skip(random).exec()
            const postVoucherHistory = await voucherHistoryModel.create({
                user: findUser._id,
                voucher: getRandomVoucher
            })
            //
            const condition = {
                user: findUser._id
            }
            // Lấy 3 bản ghi dice history mới nhất
            const getThreeTimeDice = await diceHistoryModel.find(condition).sort({ createdAt: "desc" }).limit(3)
            if (getThreeTimeDice.length === 3) {
                if (getThreeTimeDice[0].dice > 3 && getThreeTimeDice[1].dice > 3 && getThreeTimeDice[2].dice > 3) {

                    // Get the count of all users
                    const getAllPrize = await prizeModel.count().exec()
                    // Get a random entry
                    var random = Math.floor(Math.random() * getAllPrize)
                    // Again query all users but only fetch one offset by our random #
                    const getRandomPrize = await prizeModel.findOne().skip(random).exec()
                    const postPrizeHistory = await prizeHistoryModel.create({
                        user: findUser._id,
                        prize: getRandomPrize
                    })
                    const play = {
                        dice: createDiceHistory.dice,
                        voucher: getRandomVoucher,
                        prize: getRandomPrize
                    }
                    const result = await playDiceModel.create(play)
                    // Trả ra kêt quả Json
                    return res.status(200).json({
                        result
                    })
                }
                else {
                    const play = {
                        dice: createDiceHistory.dice,
                        voucher: getRandomVoucher,
                        prize: null
                    }
                    try {
                        const result = await playDiceModel.create(play)
                        // Trả ra kêt quả Json
                        return res.status(200).json({
                            result
                        })
                    } catch (error) {
                        return res.status(500).json({
                            status: "Internal Server Error",
                            message: error.message
                        })
                    }

                }
            }
            else {
                const play = {
                    dice: createDiceHistory.dice,
                    voucher: getRandomVoucher,
                    prize: null
                }
                try {
                    const result = await playDiceModel.create(play)
                    // Trả ra kêt quả Json
                    return res.status(200).json({
                        result
                    })
                } catch (error) {
                    return res.status(500).json({
                        status: "Internal Server Error",
                        message: error.message
                    })
                }
            }
        }
        // Nếu xúc sắc đổ ra bé hơn hoặc bằng  3
        else {
            const play = {
                dice: createDiceHistory.dice,
                voucher: null,
                prize: null
            }
            try {
                const result = await playDiceModel.create(play)
                // Trả ra kêt quả Json
                return res.status(200).json({
                    result
                })
            } catch (error) {
                return res.status(500).json({
                    status: "Internal Server Error",
                    message: error.message
                })
            }
        }
    }
}
// hàm sinh số ngẫu nhiên từ 1 đến 6
function rollDice(req, res) {
    return vSoBatKyTu1Den6 = Math.floor(Math.random() * 6 + 1)
}
// Export
module.exports = {
    createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteHistoryById, getAllDiceHistoryOfUser, playDice
}