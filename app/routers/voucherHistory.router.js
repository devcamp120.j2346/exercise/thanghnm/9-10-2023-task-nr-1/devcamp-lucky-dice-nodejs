const express = require("express");
const { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById, getAllVoucherHistoryOfUser } = require("../controllers/voucherHistory.controller");
const router = express.Router();

// Router Tạo
router.post("/", createVoucherHistory)
// Router Get all
router.get("/all/", getAllVoucherHistory)
router.get("/", getAllVoucherHistoryOfUser)
// Router Get by Id
router.get("/:voucherHistoryId", getVoucherHistoryById)
// Router Update by Id
router.put("/:voucherHistoryId", updateVoucherHistoryById)
// Router Delete by Id
router.delete("/:voucherHistoryId", deleteVoucherHistoryById)

module.exports = router