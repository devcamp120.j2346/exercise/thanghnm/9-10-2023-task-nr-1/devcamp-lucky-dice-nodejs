// Import express
const express = require("express")
// Import Mongoose
const mongoose = require("mongoose")
const path = require('path');
//Import Model
const userModel = require("./app/models/user.model");
const diceHistoryModel = require("./app/models/diceHistory.model");
const voucherModel = require("./app/models/voucher.model");
const prizeModel = require("./app/models/prize.model");
const playDiceModel = require("./app/models/playDice.model");
const prizeHistoryModel = require("./app/models/prizeHistory.model");
const voucherHistoryModel = require("./app/models/voucherHistory.model");

// Import router
const userRouter = require("./app/routers/user.router")
// Import router
const diceRouter = require("./app/routers/diceHistory.router")
// Import router
const prizeRouter = require("./app/routers/prize.router")
// Import router
const voucherRouter = require("./app/routers/voucher.router");
// Import router
const prizeHistoryRouter = require("./app/routers/prizeHistory.router");
const voucherHistoryRouter = require("./app/routers/voucherHistory.router");

//B2: Khởi tạo app express
const app = new express()
app.use(express.json())
// Kết nối với MongoDB:
mongoose.connect("mongodb://127.0.0.1:27017/LuckyDice")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleEror(error))
// Hiển thị hình ảnh
app.use(express.static(__dirname + "/views"))
const port = 8000

// Chạy port/ vào trang luckydice
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/views/index.html"))
})
// dùng router
app.use("/devcamp-lucky-dice/users", userRouter)
app.use("/devcamp-lucky-dice/dice-histories", diceRouter)
app.use("/devcamp-lucky-dice/prizes", prizeRouter)
app.use("/devcamp-lucky-dice/prize-histories", prizeHistoryRouter)
app.use("/devcamp-lucky-dice/vouchers", voucherRouter)
app.use("/devcamp-lucky-dice/voucher-histories", voucherHistoryRouter)



//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})